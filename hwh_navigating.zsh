
function ueu_iqi_searching_query() {
    local input
    local output
    local parse
    local tests
    tests="$1"
    # GIVEN existing directory pathway
    # AND file searching made
    input=$(ag "$tests" -l --silent)
    # WHEN selection of relevant file made
    output=$(echo "$input" | fzf -x)
    # THEN pathway of chosen file made
    parse=$(dirname "$output")
    cd "$parse"
}
